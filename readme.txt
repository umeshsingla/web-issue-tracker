## Web Based Issue Tracker 
# Full fledged project with objective to get introduced to RESTful framework, Network Requests and the difference between presentation layer and the data access layer.

## Technologies Used
# Ruby on Rails(server side)
# AngularJS(client side)

The project's objective was to make a tracker where projects can be added, issues can be created for a certain project and the discussions can be followed on a particular issue.
# Features:
1. Projects can be made public and private. Access level for a certain person can be changed from UI itself, ranging from Admin to Regular to View Only. Add members to projects and they'll sent an email invitation.
2. Issues can be assigned, closed, marked duplicate, up voted.
3. Live discussion system, comments.
4. Guest View into the website, where Guest will be able to browse through public projects only. This was handled through cookies.
5. Proper HTTP status codes, routes, APIs and practices were followed while doing.