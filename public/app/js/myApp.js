// To run this code, edit file
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to myAppName
// -----------------------------------

var myApp = angular.module('myAppName', ['angle']);

myApp.run(["$log", function ($log) {

    $log.log('running... inside module');

}]);

App.constant("MY_CONSTANT", {
    "url": "http://localhost:3000"
});

App.constant("responseCode", {
    "SUCCESS": 200
});



myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper) {
        'use strict';

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // default route
        $urlRouterProvider.otherwise('/page/login');

        // Application Routes
        $stateProvider
            // Single Page Routes
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('page.login', {
                url: '/login',
                title: "Login",
                templateUrl: 'app/pages/login.html'
            })
            .state('page.register', {
                url: '/register',
                title: "Register",
                templateUrl: 'app/pages/register.html'
            })

            //App routes
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('modernizr', 'icons', 'screenfull')
            })

            .state('app.dashboard', {
                url: '/projects',
                title: 'Dashboard',
                templateUrl: helper.basepath('dashboard.html')
            })

            .state('app.view_issues', {
                url: '/projects/{project_id}/issues',
                title: 'View Project Issues',
                templateUrl: helper.basepath('view_issues.html'),
                resolve: helper.resolveFor('ngDialog')
            })

            .state('app.view_comments', {
                url: '/projects/{project_id}/issues/{issue_id}/comments',
                title: 'View Comments on Issue',
                templateUrl: helper.basepath('view_comments.html'),
                resolve: helper.resolveFor('ngDialog')
            })
            .state('app.all_activity', {
                url: '/all_activity',
                title: 'All My Activity',
                templateUrl: helper.basepath('all_activity.html')
            })
            .state('app.create_project', {
                url: '/create_project',
                title: 'Create New Project',
                templateUrl: helper.basepath('create_project.html')
            });

    }]);
