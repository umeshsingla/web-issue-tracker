App.controller('projectIssuesController',function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT,$rootScope ,$state, $stateParams, ngDialog) {
    'use strict';


        $rootScope.checkUser();
        $rootScope.user.name = localStorage.getItem("useremail");
        $rootScope.user.id = localStorage.getItem("user_id");

    $scope.Guest= localStorage.getItem("guest");

    $scope.project_id = $stateParams.project_id;
    $scope.project_name='';
    $scope.issue_list=[];
    $scope.error = '';

    $scope.newIssue={
        id:'',
        project_id:$scope.project_id,
        name:'',
        description:'',
        access_level:'',
        tags:'',
        creator_id: localStorage.getItem("user_id")
    };
    
    $scope.createIssue = function () {

        var str="";
       for(var i=0;i<$scope.newIssue.tags.length;i++){
           var obj= $scope.newIssue.tags[i];
           str = str.concat(obj["text"]);
           str = str.concat(",")
       }

        $scope.newIssue.tags=str;

       $http.post(MY_CONSTANT.url + '/issues',
           $scope.newIssue
       ).then(
           function (data) {
               if(data.status==201){

                   $scope.newIssue.id=data.data.id;
                   alert("Issue Created Successfully.");

                   console.log($scope.newIssue.tags);
                   $scope.newIssue.tags = str.split(",");

                   $scope.issue_list.push($scope.newIssue);

                   $scope.newIssue={}
               }
               else{
                   $scope.error="Server Execution Error";
               }
           });
    };

    $scope.createNewIssueDialog = function () {

        $scope.newIssue={
            id:'',
            project_id:$scope.project_id,
            name:'',
            description:'',
            access_level:'',
            tags:'',
            creator_id: localStorage.getItem("user_id")
        };

        ngDialog.open({
            template: 'createNewIssueId',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };
    
    $scope.viewIssues = function () {

        $scope.error = '';

        $http.get(MY_CONSTANT.url + '/projects/issues/'+ $scope.project_id)
            .then(function (data) {

                console.log(data);
                if (data.data.status == 200) {

                    $scope.issue_list=[];
                    $scope.project_name='';
                    $scope.project_name = data.data.project.name;

                    console.log($scope.project_name);

                    if(data.data.issues.length != 0) {
                        data.data.issues.forEach(function (issue) {

                            if (issue.tags) {
                                issue.tags = issue.tags.split(",");
                            }
                            $scope.issue_list.push(issue);
                        });
                    }
                    else {
                        $scope.error = "No Issues for the Project Yet!"
                    }
                }
                else {
                    $scope.error = "Server Execution Error";
                }
            });
    };
    $scope.viewIssues();
});
