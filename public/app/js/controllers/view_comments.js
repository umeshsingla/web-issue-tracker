App.controller('issueCommentsController',function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, $stateParams, ngDialog, $rootScope) {
    'use strict';

        $rootScope.checkUser();
        $rootScope.user.name = localStorage.getItem("useremail");
        $rootScope.user.id = localStorage.getItem("user_id");

    $scope.Guest= localStorage.getItem("guest");

    console.log($stateParams);

    $scope.project_id = $stateParams.project_id;
    $scope.issue_id = $stateParams.issue_id;

    $scope.comment_list=[];
    $scope.error = '';

    $scope.newComment={
        id:'',
        issue_id: $scope.issue_id,
        description:'',
        creator_id:localStorage.getItem("user_id")
    };

    $scope.createComment = function () {
        if($scope.newComment.description) {

            console.log($scope.newComment);

            $http.post(MY_CONSTANT.url + '/comments',
                $scope.newComment
            ).then(
                function (data) {
                    console.log(data);
                    if (data.status == 201) {
                        $scope.newComment.id = data.data.id;
                        $scope.newComment.created_at = data.data.created_at;
                        $scope.comment_list.push($scope.newComment);

                        $scope.error = '';
                        $scope.newComment={
                            id:'',
                            issue_id: $scope.issue_id,
                            description:'',
                            creator_id: localStorage.getItem("user_id")
                        };
                    }
                    else {
                        $scope.error = "Server Execution Error";
                    }
                });
        }
        else{
            $scope.error = "Comment Can't Be Empty";
        }
    };

    $scope.createNewCommentPlace = function () {
        $scope.newComment={
            id:'',
            issue_id: $scope.issue_id,
            description:'',
            creator_id: localStorage.getItem("user_id")
        };
        $scope.comment_list.push($scope.newComment);
    };

    $scope.viewComments = function () {

        $scope.error = '';

        $http.get(MY_CONSTANT.url + '/projects/issues/'+$scope.issue_id+'/comments')
            .then(function (data) {

                console.log(data);
                if (data.data.status == 200) {

                    $scope.comment_list=[];
                    $scope.issue=data.data.issue;

                    if ($scope.issue.tags) {
                        $scope.issue.tags = $scope.issue.tags.split(",");
                    }

                    if(data.data.comments.length != 0) {
                        data.data.comments.forEach(function (comment) {
                            $scope.comment_list.push(comment);
                        });
                    }
                    else {
                        $scope.error = "No Comments for the Issue Yet!"
                    }
                }
                else {
                    $scope.error = "Server Execution Error";
                }
            });
    };
    $scope.viewComments();

    //$scope.doneupvote=localStorage.getItem("done_upvote");
    $scope.upvoted=function(votes,id){
        //console.log(votes,id)

        $http.post(MY_CONSTANT.url+'/issues/update_upvotes',
            {
                id:id,
                upvotes:votes
            }).then(function (data) {
                //console.log(data);
                $scope.issue.upvotes=votes;
                //localStorage.setItem("done_upvote","true");
                //$scope.doneupvote=localStorage.getItem("done_upvote");

            });
    };

    $scope.duplicate=function(id){
        //console.log(id);
        $http.post(MY_CONSTANT.url+'/issues/update_duplicate',
            {
                id:id,
                duplicate:1
            }).then(function (data) {
                //console.log(data);
                $scope.issue.duplicate=1;
            });
    };

    $scope.members_of_project=[];

    $scope.users_of_project = function (id) {
        $http.post(MY_CONSTANT.url+'/projects/users_of_project',
            {
                project_id:$scope.project_id

            }).then(function (data) {
                console.log(data);
                $scope.members_of_project=data.data.members;
            });
    };
    $scope.users_of_project();
    
    $scope.assign_to = function (issue_id,user) {
        console.log(issue_id,user.user_id);

        $http.post(MY_CONSTANT.url+'/issues/update_assign',
            {
                issue_id: issue_id,
                user_id: user.user_id

            }).then(function (data) {
                console.log(data);
                $scope.issue.assigned_to=user.user_id;
            });
    };

    $scope.close_issue = function (issue_id,creator_id) {
        console.log(issue_id,creator_id);

        if(Number(localStorage.getItem("user_id"))== creator_id) {

            $http.post(MY_CONSTANT.url + '/issues/update_closed',
                {
                    issue_id: issue_id,
                    user_id: creator_id

                }).then(function (data) {
                    console.log(data);
                    $scope.issue.closed = 1;
                });
        }
        else{
            alert("You can't close the issue.")
        }
    }

});
