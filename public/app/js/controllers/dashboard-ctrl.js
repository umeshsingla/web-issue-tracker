/**
 * Created by sanjay on 3/28/15.
 */

App.controller('DashboardController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, $rootScope) {

    $scope.project_list=[];


        $rootScope.checkUser();
        $rootScope.user.name = localStorage.getItem("useremail");
        $rootScope.user.id = localStorage.getItem("user_id");


    if( localStorage.getItem("guest")){

        console.log("inside public dashboard");
        $http.get(MY_CONSTANT.url + '/projects/all')
            .then(function (data) {
                $scope.project_list = data.data.details;
                console.log(data);
            })
    }
    else {
        $http.get(MY_CONSTANT.url + '/projects?creator_id=' + localStorage.getItem("user_id"))
            .then(function (data) {
                $scope.project_list = data.data.details;
                console.log(data);
            })
    }
});
