App.controller('RegisterFormController', function ($scope, $http, $state,  $cookies, $cookieStore, $rootScope) {
    $scope.account = {};
    $scope.authMsg = '';


    $scope.register = function () {
        //console.log("inside register");
        $scope.authMsg = '';
        $http
            .post('http://localhost:3000/users', {
                user : {
                    email: $scope.account.email,
                    password: $scope.account.password,
                    auth_token:"34"
                }
            })
            .then(function (data) {

                console.log(data);

                if (data.data.status!=200) {
                    $scope.authMsg = data.data.message;
                }
                else {
                    $scope.authMsg = data.data.message;

                    localStorage.setItem("useremail",data.data.email);
                    localStorage.setItem("user_id",data.data.user_id);
                    localStorage.removeItem("guest");

                    createCookie("access_token",data.data.access_token,1);

                    $state.go('app.dashboard');
                }
            }, function () {
                $scope.authMsg = 'Server Request Error';

            });
    };

});
