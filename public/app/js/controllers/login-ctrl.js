var user_id;
App.controller('LoginController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, $rootScope) {
    

    $scope.account = {};
    $scope.authMsg = '';


    $scope.loginAsGuest = function () {
        $rootScope.loggedInGuest = true;
        createCookie("access_token","fhjdgfhjdfhkdsyfgdsbfkdshui",1);
        localStorage.setItem("useremail","Guest");
        localStorage.setItem("guest","guest");
        $state.go('app.dashboard')
    };

    $rootScope.checkUser();
    $rootScope.user.name = localStorage.getItem("useremail");
    $rootScope.user.id = localStorage.getItem("user_id");

    $scope.loginAdmin = function () {
        $scope.authMsg = '';
        $.post(MY_CONSTANT.url + '/users/login',
            {
                email: $scope.account.email,
                password: $scope.account.password,
                auth_token: ""
            }).then(
            function (data) {
                //console.log(data);
                //data = JSON.parse(data);

                if (data.status != 200) {
                    $scope.authMsg = data.message;
                    $scope.$apply();
                }
                else {

                    localStorage.setItem("useremail",data.email);
                    localStorage.setItem("user_id",data.user_id);
                    localStorage.removeItem("guest");

                    createCookie("access_token",data.access_token,1);
                    $state.go('app.dashboard');
                }
            });
    };

/*    $scope.recover = function () {

        $.post(MY_CONSTANT.url + '/forgot_password',
            {
                email: $scope.account.email
            }).then(
            function (data) {
                data = JSON.parse(data);
                console.log(data);
                if (data.status == 200) {
                    $scope.successMsg = data.message.toString();
                } else {
                    $scope.errorMsg = data.message.toString();

                }
                $scope.$apply();
            })
    };*/

    $scope.logout = function () {
        localStorage.removeItem("guest");
        endUser("access_token");
        $state.go('page.login');
    }
});

