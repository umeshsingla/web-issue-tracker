/**
 * Created by umeshksingla on 5/9/15.
 */
App.controller('allMyIssuesController',function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT,$rootScope) {
    'use strict';


    $rootScope.checkUser();
    $rootScope.user.name = localStorage.getItem("useremail");
    $rootScope.user.id = localStorage.getItem("user_id");

    $scope.Guest= localStorage.getItem("guest");

    $scope.issue_list=[];
    $scope.error = '';


    $scope.viewMyIssues = function () {

        $scope.error = '';

        $http.get(MY_CONSTANT.url + '/issues/assigned_issues/'+ localStorage.getItem("user_id"))
            .then(function (data) {

                console.log(data);
                if (data.data.status == 200) {

                    $scope.issue_list=[];

                    if(data.data.issues.length != 0) {
                        data.data.issues.forEach(function (issue) {

                            if (issue.tags) {
                                issue.tags = issue.tags.split(",");
                            }
                            $scope.issue_list.push(issue);
                        });
                    }
                    else {
                        $scope.error = "No Issues for You Yet!"
                    }
                }
                else {
                    $scope.error = "Server Execution Error";
                }
            });
    };
    $scope.viewMyIssues();
});
