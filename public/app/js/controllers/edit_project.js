App.controller('editProjectController',function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, $rootScope) {
    'use strict';

    $scope.error='';
    $scope.Guest = localStorage.getItem("guest");
    $rootScope.checkUser();
    $rootScope.user.name = localStorage.getItem("useremail");
    $rootScope.user.id = localStorage.getItem("user_id");

    $scope.project = {
        name: "",
        creator_id: localStorage.getItem("user_id"),
        description: "",
        write_members: [],
        access_level: 1
    };


    $scope.createNewProject = function () {

        console.log($scope.project);

        $http.post(MY_CONSTANT.url + '/projects',
            $scope.project
        ).then(
            function (data) {
                console.log(data);
                if (data.status == 200) {
                    alert("Project Created Successfully.");
                    $state.go('app.dashboard')
                }
                else {
                    $scope.error = "Server Execution Error";
                }
            }
        )

    };

});
