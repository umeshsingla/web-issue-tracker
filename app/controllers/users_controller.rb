class UsersController < ApplicationController

  before_action :set_user, :cors_preflight_check, only: [:show, :update, :destroy]
  after_filter :cors_set_access_control_headers


  def cors_set_access_control_headers
  headers['Access-Control-Allow-Origin'] = '*'
  headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
  headers['Access-Control-Max-Age'] = "1728000"
  end

# If this is a preflight OPTIONS request, then short-circuit the
# request, return only the necessary headers and return an empty
# text/plain.

  def cors_preflight_check
  if request.method == :options
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
    headers['Access-Control-Max-Age'] = '1728000'
    render :text => '', :content_type => 'text/plain'
  end

  end

  def login
    #login api
    require 'securerandom'
    random_string = SecureRandom.hex

    logged_in_user = User.find_by_email(params[:email])

    if logged_in_user
      #print params[:password]
      #print logged_in_user.password
      if logged_in_user.password == params[:password]

        logged_in_user.auth_token=random_string
        logged_in_user.save

        render json: {
                   user_id:logged_in_user.id,
                   email:logged_in_user.email,
                   access_token: random_string,
                   status: 200,
                   message:"User matched successfully."
               }
      else
        render json: {
                   status:404,
                   message:"Wrong Password."
               }
      end
    else
      render json: {
                 status:404,
                 message:"Wrong Email."
             }
    end
    #print logged_in_user
  end

  # GET /users
  # GET /users.json

  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  # GET /users/1.json
  def show
    render json: @user
  end


  # POST /users
  # POST /users.json
  def create
    #register api
    logged_in_user = User.find_by_email(params[:user][:email])

    if logged_in_user
      render json: {
                 access_token: logged_in_user.email,
                 status: 409,
                 message:"Email already exists."
             }
    else
      @user = User.new(user_params)

      if @user.save
        render json: {
                   user_id:@user.id,
                   email:@user.email,
                   access_token: SecureRandom.hex,
                   status: 200,
                   message:"User created successfully."
               }
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  def isloggedin

    u=User.find_by_auth_token(params[:token])
    print params
    if u
      render json: {
                 status: 200
             }
    else
      render json: {
                 status: 404
             }
    end

  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :password, :auth_token)
    end

    def allow_iframe
	    response.headers.except! 'X-Frame-Options'
    end

end
