class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index

    projects_users = ProjectUserAccessLevel.where(:user_id=>params[:creator_id]).to_a   #creator_id is basically user_id, nothing to do with the word 'creator'

    project_details=[]

    projects_users.each do |project_user|

      p=Project.find(project_user.project_id)

      obj={
          "name":p.name,
          "description":p.description,
          "updated_at":p.updated_at,
          "id":p.id,
          "creator_id":p.creator_id,
          "access_level":project_user.access_level,
          "public_or_private":p.access_level,
          "user_id":project_user.user_id
      }

      project_details.append(obj)

    end

    render json: {
               details:project_details
           }
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    render json: @project
  end

  # POST /projects
  # POST /projects.json
  def create

    @project = Project.new(project_params)

    # finding the creator of the current project
    user=User.find(@project.creator_id)

    if @project.save && ProjectUserAccessLevel.create(:user => user, :project => @project, :access_level => 10)

      render json: {
                 status: 201,
                 message: "Project Created successfully"
             }
    else
      render json: @project.errors, status: :unprocessable_entity
    end

    # adding the project to all the members on the basis of access_levels
    # if user is not there, he is invited to sign up but till then access_level remains 0

    #for write enabled members
    if params[:write_members]
      params[:write_members].each do |member|

        if member["text"] != user.email
          u = User.find_by_email(member["text"])

          if u
            if u.email!=user.email
               ProjectUserAccessLevel.create(:user => u, :project => @project, :access_level => 5)
            end
          else
            i=sendmail(member["text"])
            puts i
          end
        end
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    @project = Project.find(params[:id])

    if @project.update(project_params)
      head :no_content
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy

    head :no_content
  end

  def issues
    p = Project.find(params[:id])
    i = Issue.where(:project_id => params[:id])
    if i
      render json: {
               project:p,
               issues:i,
               status: 200,
               message:"Fetched Issues Successfully."
           }
    else
      render json: {
                 status: 404,
                 message:"Can not fetch issues."
             }
    end
  end

  def allpublic
    p=Project.where(:access_level => 2)
    if p
      render json:{
                 details:p,
                 status:200
             }
    else
      render json:{
                 status:404
             }
    end
  end

  def sendmail (email)
    puts "sending emails", email
    recipient = email
    subject = "You need to sign up"
    message = "sghjdghj"
    Emailer.contact(recipient, subject, message)
    puts "sent"
    return true
  end

  def users_of_project
    p=ProjectUserAccessLevel.where(:project_id => params[:project_id])
    render json:{
               members:p,
               status:200,
               message:"Users fetched"
           }
  end

  private

    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.permit( :creator_id, :name, :description, :access_level)
    end
end
