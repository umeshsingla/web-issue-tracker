class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :update, :destroy]

  # GET /issues
  # GET /issues.json
  def index
    @issues = Issue.all

    render json: @issues
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    render json: @issue
  end

  # POST /issues
  # POST /issues.json
  def create

    @issue = Issue.new(new_issue_params)

    if @issue.save
      render json: @issue, status: :created
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    @issue = Issue.find(params[:id])

    if @issue.update(issue_params)
      head :no_content
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  def update_upvotes

    @issue = Issue.find(params[:id])

    if @issue.update(:upvotes=>params[:upvotes])
      render json:{
                 status: 200,
                 message: "upvoted"
             }
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  def update_duplicate
    @issue = Issue.find(params[:id])

    if @issue.update(:duplicate=>params[:duplicate])
      render json:{
                 status: 200,
                 message: "duplicated"
             }
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  def update_assign

    @issue = Issue.find(params[:issue_id])

    if @issue.update(:assigned_to=>params[:user_id])
      render json:{
                 status: 200,
                 message: "assigned"
             }
    else
      render json: @issue.errors, status: :unprocessable_entity
    end

  end

  def update_closed

    @issue = Issue.find(params[:issue_id])

    if @issue.update(:closed=>1)
      render json:{
                 status: 200,
                 message: "closed"
             }
    else
      render json: @issue.errors, status: :unprocessable_entity
    end

  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy

    head :no_content
  end

  def comments
    i = Issue.find(params[:id])
    c = Comment.where(:issue_id =>  params[:id])
    if c
      render json: {
                 issue: i,
                 comments: c,
                 status: 200,
                 message:"Fetched Comments Successfully."
             }
    else
      render json: {
                 status: 404,
                 message:"Cant fetch comments."
             }
    end
  end

  def assigned_issues

    p = Issue.where(:assigned_to => params[:user_id])
    puts params
    render json:{
               issues:p,
               status:200,
               message:"Issues fetched"
           }
  end


  private

    def set_issue
      @issue = Issue.find(params[:id])
    end

    def issue_params
      params[:issue]
    end

    def new_issue_params
      params.permit( :creator_id, :project_id, :name, :description, :access_level, :tags, :assigned_to)
    end
end
