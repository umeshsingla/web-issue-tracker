class Project < ActiveRecord::Base
  has_many :project_user_access_levels
  has_many :users, :through => :project_user_access_levels
  has_many :issues
end