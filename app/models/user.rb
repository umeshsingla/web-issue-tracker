class User < ActiveRecord::Base
  has_many :project_user_access_levels
  has_many :projects, :through => :project_user_access_levels
end
