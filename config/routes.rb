Rails.application.routes.draw do

  post 'users/login', to: 'users#login'
  post 'users/isloggedin', to: 'users#isloggedin'
  post 'issues/update_upvotes', to: 'issues#update_upvotes'
  post 'issues/update_duplicate', to: 'issues#update_duplicate'
  post 'issues/update_assign', to: 'issues#update_assign'
  post 'issues/update_closed', to: 'issues#update_closed'
  post 'projects/users_of_project', to: 'projects#users_of_project'
  get 'issues/assigned_issues/:user_id', to: 'issues#assigned_issues'
  get 'projects/issues/:id', to: 'projects#issues'
  get 'projects/all', to: 'projects#allpublic'
  get 'projects/issues/:id/comments', to: 'issues#comments'
  resources :issues, except: [:new, :edit]
  resources :projects, except: [:new, :edit]
  resources :comments, except: [:new, :edit]
  resources :posts, except: [:new, :edit]
  resources :users, except: [:new, :edit]


  #options 'users' => 'users#create'
#  root 'pages#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
