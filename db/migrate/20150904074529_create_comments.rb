class CreateComments < ActiveRecord::Migration
  def up
    create_table :comments do |t|
      t.references :issue
      t.integer :creator_id, null: false
      t.text :description
      t.timestamps null: false
    end
    add_index :comments, ["issue_id","creator_id"]

  end
  def down
    drop_table :comments
  end
end
