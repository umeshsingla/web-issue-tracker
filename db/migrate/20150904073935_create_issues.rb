class CreateIssues < ActiveRecord::Migration
  def up
    create_table :issues do |t|
      t.references :project       #project id
      t.integer :creator_id, null: false    #who created the issue
      t.string :name, null: false   #name of issue
      t.text :description   #describe the issue
      t.string :tags    #tags for the issue
      t.integer :assigned_to    #to whom it has been assigned to
      t.integer :access_level   #public/private
      t.integer :duplicate, default: 0  #marked duplicate or not
      t.integer :upvotes, default: 0  #how many upvotes
      t.integer :downvotes, default: 0  #how many downvotes
      t.timestamps null: false
    end
    add_index :issues, ["assigned_to","tags","project_id"]
    add_column :issues, :closed, :integer, :default => 0
  end
  def down
    drop_table :issues
  end
end
