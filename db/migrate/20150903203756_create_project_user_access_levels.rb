class CreateProjectUserAccessLevels < ActiveRecord::Migration
  def up
    create_table :project_user_access_levels do |t|

      t.references :user
      t.references :project
      t.integer :access_level
      t.timestamps null: false
    end
    add_index :project_user_access_levels, ["user_id","project_id"]
  end
  def down
    drop_table :project_user_access_levels
  end
end
