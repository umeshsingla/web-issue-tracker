class CreateProjects < ActiveRecord::Migration
  def up
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.timestamps null: false
    end
    add_column :projects, :creator_id, :integer, :null => false, :default => 0
    add_column :projects, :access_level, :integer, :null => false, :default => 1
  end

  def down
    drop_table :projects
  end
end
