class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.column "email", :string, null:false
      t.string :password, null: false
      t.string :auth_token, default:""
      t.timestamps null: false
    end
    add_index("users","email",unique: true)
  end

  def down
    drop_table :users
  end
end
