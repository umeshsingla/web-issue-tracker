# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150904074529) do

  create_table "comments", force: :cascade do |t|
    t.integer  "issue_id"
    t.integer  "creator_id",  null: false
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "comments", ["issue_id", "creator_id"], name: "index_comments_on_issue_id_and_creator_id"

  create_table "issues", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "creator_id",               null: false
    t.string   "name",                     null: false
    t.text     "description"
    t.string   "tags"
    t.integer  "assigned_to"
    t.integer  "access_level"
    t.integer  "duplicate",    default: 0
    t.integer  "upvotes",      default: 0
    t.integer  "downvotes",    default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "closed",       default: 0
  end

  add_index "issues", ["assigned_to", "tags", "project_id"], name: "index_issues_on_assigned_to_and_tags_and_project_id"

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id"

  create_table "project_user_access_levels", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "project_id"
    t.integer  "access_level"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "project_user_access_levels", ["user_id", "project_id"], name: "index_project_user_access_levels_on_user_id_and_project_id"

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "creator_id",   default: 0, null: false
    t.integer  "access_level", default: 1, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                   null: false
    t.string   "password",                null: false
    t.string   "auth_token", default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
